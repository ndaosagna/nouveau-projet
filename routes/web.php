<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::resource('products','ProductController');

Route::get('product','ProductController@index')->name('products.index');

Route::get('create/product',[
    'as' => 'products.create',
    'uses' => 'ProductController@create'
]);


Route::post('product.store','ProductController@store')->name('products.store');
Route::get('product/show/{product}','ProductController@show')->name('products.show');
Route::get('product/edit/{product}','ProductController@edit')->name('products.edit');
Route::put('product/update/{product}','ProductController@update')->name('products.update');
Route::delete('product/destroy/{product}','ProductController@destroy')->name('products.destroy');